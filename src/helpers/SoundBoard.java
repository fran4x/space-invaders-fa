package helpers;

import java.io.IOException;
import java.util.HashMap;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.openal.SoundStore;
import org.newdawn.slick.util.ResourceLoader;

public class SoundBoard {
	
	private static HashMap<String, Audio> soundTable;
	private static boolean muted;
	
	public static void init(){
		muted = false;
		soundTable = new HashMap<String, Audio>();
		addSound("explosion1",QuickLoadSound("explosion1"));
		addSound("shot",QuickLoadSound("shot"));
		addSound("rock_cracking",QuickLoadSound("rock_cracking"));
		addSound("missile",QuickLoadSound("missile"));
		addSound("boatHorn",QuickLoadSound("boatHorn"));
		addSound("bonus");
		addSound("click");
		addSound("gameover");
	}
	
	public static Audio QuickLoadSound(String resName){
		Audio sound = null;
		try{
			sound = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res\\"+resName+".wav"));
		} catch(IOException e){
			e.printStackTrace();
		}
		return sound;
	}
	
	public static void addSound(String name, Audio sound){
		soundTable.put(name, sound);
	}
	
	public static void addSound(String name){
		soundTable.put(name, QuickLoadSound(name));
	}
	
	public static void playSound(String name){
		playSound(name,1.0f);
	}
	
	public static void playSound(String name, float volume){
		if(soundTable.containsKey(name)){
			soundTable.get(name).playAsSoundEffect(1.0f, volume, false);
		} else {
			System.out.println("Sound not found");
		}
		SoundStore.get().poll(0);
	}
	
	public static void toggleMute(){
		muted = !muted;
		if(muted){
			SoundStore.get().setSoundVolume(-999);
		} else {
			SoundStore.get().setSoundVolume(1);
		}
		
	}
}
