package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class HighScoreScribe {
	
	public static void writeHighScore(int highScore){
		try {
			File file = new File("highscore");
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(String.valueOf(highScore));
			bw.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static int readHighScore(){
		int highScore = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader("highscore"));
			highScore = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
		return highScore;
	}
}
