package helpers;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.io.IOException;
import java.io.InputStream;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;



public class Artist {
	
	
	public static void DrawQuad(float x, float y, float width, float height){
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x+width,y);
		glVertex2f(x+width, y+height);
		glVertex2f(x, y+height);
		glEnd();
	}
	
	public static void DrawQuadTex(Texture tex, float x, float y, float offsetX, float offsetY, float width, float height){
		float normalWUpper = ((float)offsetX+width)/(float)tex.getTextureWidth();
		float normalWLower = ((float)offsetX)/(float)tex.getTextureWidth();
		float normalHUpper = ((float)offsetY+height)/(float)tex.getTextureHeight();
		float normalHLower = ((float)offsetY)/(float)tex.getTextureHeight();
		
		tex.bind();
		glTranslatef(x,y,0);
		glBegin(GL_QUADS);
		glTexCoord2f(normalWLower,normalHLower);
		glVertex2f(0,0);
		glTexCoord2f(normalWUpper,normalHLower);
		glVertex2f(width,0);
		glTexCoord2f( normalWUpper , normalHUpper);
		glVertex2f(width,height);
		glTexCoord2f(normalWLower,normalHUpper);
		glVertex2f(0,height);
		
		glEnd();
		glLoadIdentity();
	}
	
	public static void DrawQuadTex(Texture tex, float x, float y, float width, float height){
		DrawQuadTex(tex, x, y, 0, 0, width, height);
	}
	
	public static void DrawQuadTexRot(Texture tex, float x, float y, float width, float height, float angle){
		tex.bind();
		glTranslatef(x + width/2,y + height/2,0);
		glRotatef(angle,0,0,1);
		glTranslatef(-width/2,-height/2,0);
		glBegin(GL_QUADS);
		glTexCoord2f(0,0);
		glVertex2f(0,0);
		glTexCoord2f(1,0);
		glVertex2f(width,0);
		glTexCoord2f(1,1);
		glVertex2f(width,height);
		glTexCoord2f(0,1);
		glVertex2f(0,height);
		
		glEnd();
		glLoadIdentity();
	}
	
	public static Texture LoadTexture(String path, String fileType){
		Texture tex = null;
		InputStream in = ResourceLoader.getResourceAsStream(path);
		try {
			tex = TextureLoader.getTexture(fileType, in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tex;
	}
	
	public static Texture QuickLoad(String name){
		Texture tex = null;
		tex = LoadTexture("res/"+name+".png","PNG");
		return tex;
	}
	
}
