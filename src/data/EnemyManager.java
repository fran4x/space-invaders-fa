package data;

import static helpers.Artist.*;
import static helpers.Clock.*;
import static helpers.SoundBoard.playSound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.newdawn.slick.opengl.Texture;

import helpers.Clock;

public class EnemyManager {
	private ArrayList<Enemy> enemies;
	private float speed;
	private int movementModifier;
	private Texture texture;
	private ProjectileManager projectileManager;
	private float shotProbability;
	private Game game;
	private float offsetY;
	private int jumps;
	
	public EnemyManager(float offsetY, float speed, Texture texture, ProjectileManager projectileManager, float shotProbability, Game game) {
		this.offsetY = offsetY;
		this.speed = speed;
		this.enemies = new ArrayList<Enemy>();
		this.texture = texture;
		this.projectileManager = projectileManager;
		this.movementModifier = 1;
		this.shotProbability = shotProbability;
		this.game = game;
		this.jumps = 0;
		this.populate();
	}
	
	public void update(){
		removeDead();
		move();
		if(!Clock.isPaused()){
			shoot();
		}
		for(Enemy e : this.enemies){
			e.update();
		}
	}
	
	private void move(){
		increaseEnemiesX(Delta()*speed * movementModifier);
		if(this.getMaxX()>800-48&&movementModifier==1){
			movementModifier *= -1;
			speed += 5;
			jumps++;
			while(this.getMaxX()>800-48){
				this.increaseEnemiesX(-5);
			}
			this.increaseEnemiesY(16);
		}
		if(this.getMinX()<16&&this.movementModifier==-1){
			movementModifier *= -1;
			speed += 5;
			jumps++;
			this.increaseEnemiesY(16);
		}
		
	}
	
	public boolean playerKill(){
		for(Enemy e : enemies){
			if(e.getY()>475){
				return true;
			}
		}
		return false;
	}
	
	public void shoot(){
		for(Enemy e : this.getLeaders()){
			int prob = new Random().nextInt(1000000);
			if(prob<1000*shotProbability){
				e.shoot();
				playSound("missile");
			}
		}
	}
	
	private ArrayList<Enemy> getLeaders(){
		HashMap<Integer, Enemy> leadersMap = new HashMap<Integer, Enemy>();
		
		for(Enemy e : enemies){
			if(!leadersMap.containsValue(e.getX())||
					e.getY()>leadersMap.get(e.getX()).getY()){
				leadersMap.put((int)e.getX(), e);
			}
		}
		
		ArrayList<Enemy> leaders = new ArrayList<Enemy>();
		for(Enemy e : leadersMap.values()){
			leaders.add(e);
		}
		return leaders;
	}
	
	private float getMaxX(){
		float maxN = 0;
		
		for(Enemy e : enemies){
			if(e.getX()>maxN){
				maxN = e.getX();
			}
		}
		
		return maxN;
	}
	
	private float getMinX(){
		float minN = 800;
		
		for(Enemy e : enemies){
			if(e.getX()<minN){
				minN = e.getX();
			}
		}
		
		return minN;
	}
	
	private float getMinY(){
		float minY = 600;
		
		for(Enemy e: enemies){
			if(e.getY()<minY){
				minY = e.getY();
			}
		}
		
		return minY;
	}
	
	private void increaseEnemiesX(float dx){
		for(Enemy e : enemies){
			e.setX(e.getX()+dx);
		}
	}
	
	private void increaseEnemiesY(float dy){
		for(Enemy e : enemies){
			e.setY(e.getY()+dy);
		}
	}
	
	private void populate(){
		for(int y = 64+(int)offsetY;y<250+(int)offsetY;y+=40){
			for(int x = 128; x<800-128;x+=64){
				enemies.add(new Enemy(x, y, texture, projectileManager,game));
			}
		}
	}
	
	private void removeDead(){
		ArrayList<Enemy> toBeRemoved = new ArrayList<Enemy>();
		for(Enemy e : enemies){
			if(!e.isAlive()){
				toBeRemoved.add(e);
				game.score += 30;
			}
		}
		enemies.removeAll(toBeRemoved);
	}

	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}
	
	public boolean allDead(){
		return enemies.isEmpty();
	}

	public float getOffsetY() {
		return offsetY;
	}

	public int getJumps() {
		return jumps;
	}
	
	
	
}
