package data;

import helpers.Artist;
import helpers.SoundBoard;
import ui.GameOver;

public class DeathTransition implements State {
	
	private Game game;
	private EffectManager effectManager;
	
	

	public DeathTransition(Game game, EffectManager effectManager) {
		this.game = game;
		this.effectManager = effectManager;
		effectManager.addAnimation(new Animation(Artist.QuickLoad("explosion"), game.player.getX(), game.player.getY(), 32, 32, 0.01f));
		SoundBoard.playSound("explosion1");
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		game.drawBackground();
		effectManager.update();
		if(effectManager.noOngoingAnim()){
			if(game.lives>=0){
				Boot.currentState = game;
			} else {
				Boot.currentState = new GameOver(game.score);
			}
		}
		
	}
	
}
