package data;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

import java.util.ArrayList;

import static helpers.Artist.*;
import static helpers.Clock.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import helpers.Artist;
import helpers.Clock;
import helpers.SoundBoard;
import ui.GameOverlay;

public class Game implements State {
	
	private static Texture background = Artist.QuickLoad("space");
	public EffectManager effectManager;
	public Player player;
	public EnemyManager enemyManager;
	public CoverManager coverManager;
	public MothershipGenerator mothershipGenerator;
	public ProjectileManager projectileManager;
	public GameOverlay gameOverlay;
	public int lives = 3;
	public int level = 1;
	public int score = 0;
	private float offsetY = 0;
	
	public Game(){
		this.effectManager = new EffectManager();
		
		this.background = QuickLoad("space");
		newGame();
		coverManager = new CoverManager();
		
	}
	
	public void update(){
		processInput();
		drawBackground();
		enemyManager.update();
		mothershipGenerator.update();
		player.update();
		coverManager.update();
		projectileManager.update();
		effectManager.update();
		gameOverlay.update();
		if(enemyManager.playerKill()){
			player.die();
		}
		
		if(!player.isAlive()){
			lives--;
			Boot.currentState = new DeathTransition(this, effectManager);
			newLife();
			
		}
		
		if(enemyManager.allDead()){
			level++;
			lives++;
			offsetY = enemyManager.getJumps()*16;
			newGame();
		}
	}
	
	private void newGame(){
		projectileManager = new ProjectileManager(this);
		
		enemyManager = new EnemyManager(offsetY,level*10,QuickLoad("enemyFighter"),projectileManager,(level/3)+1,this);
		
		mothershipGenerator = new MothershipGenerator(30, 75, QuickLoad("mothership"),this);
		
		gameOverlay = new GameOverlay(this);
		
		if(level%7==0){
			coverManager = new CoverManager();
		}
		newLife();
	}
	
	private void newLife(){
		player = new Player(400-32, 550, 125+(50*(level/5)), 1*(int)Math.floor(level/10)+1, QuickLoad("fighter32"), projectileManager);
	}
	
	private void processInput(){
		if(Keyboard.isKeyDown(Keyboard.KEY_A)){
			player.moveLeft();
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_D)){
			player.moveRight();
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			player.shoot();
		}
		while(Keyboard.next()){
			if(Keyboard.getEventKey()==Keyboard.KEY_P&&Keyboard.getEventKeyState()){
				Clock.Pause();
			}
			if(Keyboard.getEventKey()==Keyboard.KEY_F&&Keyboard.getEventKeyState()){
				Boot.setFullscreen();
			}
			if(Keyboard.getEventKey()==Keyboard.KEY_M&&Keyboard.getEventKeyState()){
				SoundBoard.toggleMute();
			}
		}
		
	}
	
	
	public void drawBackground(){
		
		DrawQuadTex(background,0,0,1024,1024);
	}
	
	public static void drawBackgroundStat(){
		
		DrawQuadTex(background,0,0,1024,1024);
	}
}
