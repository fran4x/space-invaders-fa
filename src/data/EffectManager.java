package data;

import java.util.ArrayList;

import org.newdawn.slick.opengl.Texture;

public class EffectManager {
	private ArrayList<Animation> animations;
	
	public EffectManager(){
		this.animations = new ArrayList<Animation>();
	}
	
	public void update(){
		updateAnimations();
	}
	
	public void addAnimation(Animation animation){
		animations.add(animation);
	}
	
	private void updateAnimations(){
		for(Animation a : this.animations){
			a.update();
		}
		
		ArrayList<Animation> toBeRemoved = new ArrayList<Animation>();
		for(Animation a : this.animations){
			if(!a.isContinues()){
				toBeRemoved.add(a);
			}
		}
		this.animations.removeAll(toBeRemoved);
	}
	
	public boolean noOngoingAnim(){
		return animations.isEmpty();
	}
	
}
