package data;

import static helpers.Artist.DrawQuadTex;
import static helpers.Clock.*;

import java.util.ArrayList;

import org.newdawn.slick.opengl.Texture;

public class Projectile {
	
	private float x;
	private float y;
	private float speed;
	private Texture texture;
	private boolean active;
	private Game game;
	private ArrayList<Enemy> enemies;
	private ArrayList<CoverPiece> pieces;
	private MothershipGenerator mothershipGenerator;
	private Player player;
	private boolean playerProjectile;
	
	public Projectile(float x, float y, float speed, Texture texture, Game game, boolean playerProjectile) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.texture = texture;
		this.active = true;
		this.enemies = game.enemyManager.getEnemies();
		this.pieces = game.coverManager.getAllPieces();
		this.mothershipGenerator = game.mothershipGenerator;
		this.player = game.player;
		this.playerProjectile = playerProjectile;
	}
	
	public void update(){
		if(playerProjectile){
			checkEnemyCollision();
		} else {
			checkPlayerCollision();
		}
		checkCoverPieceCollision();
		checkMotherShipCollision();
		
		
		if(y<-16||y>600){
			active = false;
		}
		if(active){
			y-=Delta()*speed;
			
			DrawQuadTex(texture, x, y, 16, 16);
		}
	}
	
	private void checkEnemyCollision(){
		for(Enemy e : enemies){
			if(e.checkCollision(this)){
				active = false;
				break;
			}
		}
	}
	
	private void checkCoverPieceCollision(){
		for(CoverPiece p : this.pieces){
			if(p.checkProjectileCollision(this)){
				active = false;
				break;
			}
		}
	}
	
	private void checkMotherShipCollision(){
		if(mothershipGenerator.getMothership().isAlive()
				&&mothershipGenerator.getMothership().checkCollisionWithProjectile(this)){
			active = false;
		}
	}
	
	private void checkPlayerCollision(){
		if(player.checkCollisionWithProjectile(this)){
			this.active = false;
		}
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isPlayerProjectile(){
		return playerProjectile;
	}
	
}
