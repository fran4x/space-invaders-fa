package data;

import java.util.ArrayList;

import org.newdawn.slick.opengl.Texture;

import helpers.Clock;

public class ProjectileManager {
	
	ArrayList<Projectile> projectiles;
	Game game;
	
	public ProjectileManager(Game game) {
		this.projectiles = new ArrayList<Projectile>();
		this.game = game;
	}
	
	public void update(){
		this.removeDead();
		for(Projectile p : projectiles){
			p.update();
		}
	}
	
	private void removeDead(){
		ArrayList<Projectile> toBeRemoved = new ArrayList<Projectile>();
		for(Projectile p : projectiles){
			if(!p.isActive()){
				toBeRemoved.add(p);
			}
		}
		projectiles.removeAll(toBeRemoved);
	}
	
	public void addProjectile(float x, float y, float speed, Texture texture, boolean playerProjectile){
		if(!Clock.isPaused()){
			projectiles.add(new Projectile(x, y, speed, texture, game, playerProjectile));
		}
	}
	
	public int playerProjectiles(){
		int number = 0;
		
		for(Projectile p : projectiles){
			if(p.isPlayerProjectile()){
				number++;
			}
		}
		
		return number;
	}
	
}
