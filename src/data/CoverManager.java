package data;

import java.util.ArrayList;

public class CoverManager {
	
	private ArrayList<CoverPiece> pieces;
	
	public CoverManager() {
		this.pieces = new ArrayList<CoverPiece>();
		this.generateSets();
	}
	
	private void generateSets(){
		for(int x = 78; x<800;x+=200){
			pieces.addAll(new CoverSet(x, 475).getPieces());
		}
	}
	
	public void update(){
		removeFinished();
		
		for(CoverPiece p : pieces){
			p.update();
		}
	}
	
	private void removeFinished(){
		ArrayList<CoverPiece> toBeRemoved = new ArrayList<CoverPiece>();
		for(CoverPiece p : pieces){
			if(!p.isAlive()){
				toBeRemoved.add(p);
			}
		}
		pieces.removeAll(toBeRemoved);
	}
	
	public ArrayList<CoverPiece> getAllPieces(){
		
		return pieces;
	}
	
}
