package data;

public interface State {
	
	public void update();
	
}
