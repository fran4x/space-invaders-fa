package data;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.ImageIOImageData;

import helpers.Clock;
import helpers.HighScoreScribe;
import helpers.SoundBoard;
import ui.GameOver;
import ui.MainMenu;

public class Boot {
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public static boolean gameContinues=true;
	public static State currentState;
	
	public Boot() {
		initWindow();
		SoundBoard.init();
		currentState = new MainMenu();
		loop();
	}
	
	public static void main(String[] args){
		new Boot();
	}
	
	private void loop(){
		while(!Display.isCloseRequested()&&gameContinues&&!Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glMatrixMode(GL_MODELVIEW);
            Color.white.bind();
            glLoadIdentity();
            
    		Clock.update();
            
            currentState.update();
            processInput();
            
            Display.update();
			Display.sync(100);
		}
		
		Display.destroy();
		AL.destroy();
	}
	
	private void processInput(){
		while(Keyboard.next()){
			if(Keyboard.getEventKey()==Keyboard.KEY_F&&Keyboard.getEventKeyState()){
				setFullscreen();
			}
			if(Keyboard.getEventKey()==Keyboard.KEY_M&&Keyboard.getEventKeyState()){
				SoundBoard.toggleMute();
			}
		}
	}
	
	private void initWindow(){
		Display.setTitle("Space Invaders: FA");
		
		try {
			
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.create();
			Display.setIcon(new ByteBuffer[] {
	                new ImageIOImageData().imageToByteBuffer(ImageIO.read(getClass().getResource("/res/fighter16.png")), false, false, null),
	                new ImageIOImageData().imageToByteBuffer(ImageIO.read(getClass().getResource("/res/fighter32.png")), false, false, null)
	                });
		} catch (LWJGLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0,WIDTH,HEIGHT,0,1,-1);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		glDisable(GL_DEPTH_TEST);  
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	}
	
	public static void setFullscreen(){
		DisplayMode displayMode = null;
        DisplayMode[] modes = null;
		try {
			modes = Display.getAvailableDisplayModes();
		} catch (LWJGLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

         for (int i = 0; i < modes.length; i++)
         {
             if (modes[i].getWidth() == WIDTH
             && modes[i].getHeight() == HEIGHT
             && modes[i].isFullscreenCapable())
               {
                    displayMode = modes[i];
               }
         }
         try {
        	
        	Display.setDisplayMode(displayMode);
        	Display.setVSyncEnabled(true);
			Display.setFullscreen(!Display.isFullscreen());
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
