package data;

import static helpers.Artist.*;
import static helpers.Clock.*;

import org.newdawn.slick.opengl.Texture;

public class Animation {
	private Texture spriteSheet;
	private float x, y, width,height, speed;
	private float offsetX, offsetY;
	private float timeSinceLastFrame;
	private boolean continues;
	
	public Animation(Texture spriteSheet, float x, float y, float width, float height, float speed) {
		this.spriteSheet = spriteSheet;
		this.width = width;
		this.height = height;
		this.speed = speed;
		this.x = x;
		this.y = y;
		this.timeSinceLastFrame=0;
		this.offsetX=0;
		this.offsetY=0;
		this.continues = true;
	}
	
	public void update(){
		if(this.continues){
			timeSinceLastFrame+=Delta();
			
			DrawQuadTex(spriteSheet, x, y, offsetX, offsetY, width, height);
			
			advanceFrame();
		}
	}
	
	private void advanceFrame(){
		while(timeSinceLastFrame>speed){
			timeSinceLastFrame-=speed;
			offsetX+=width;
			if(offsetX>=spriteSheet.getImageWidth()){
				offsetX=0;
				offsetY+=height;
				if(offsetY>=spriteSheet.getImageHeight()){
					this.continues = false;
				}
			}
			
		}
		
	}

	public boolean isContinues() {
		return continues;
	}
	
}
