package data;

import org.newdawn.slick.opengl.Texture;

import helpers.Clock;
import helpers.SoundBoard;

import static helpers.Artist.*;
import static helpers.Clock.*;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Player {
	
	private float x;
	private float y;
	private float speed;
	private int maxShots;
	private boolean alive;
	private Texture texture;
	private ProjectileManager projectileManager;
	
	public Player(float x, float y, float speed, int maxShots, Texture texture, ProjectileManager projectileManager) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.maxShots=maxShots;
		this.texture=texture;
		this.alive = true;
		this.projectileManager = projectileManager;
	}
	
	public void update(){
		if(alive){
			DrawQuadTex(texture, x, y, 32, 32);
		}
	}
	
	
	public void moveLeft(){
		if(x>=0&&x!=0){
			x-=Delta()*speed;
		} else {
			x = 0;
		}
	}
	
	public void moveRight(){
		if(x<=800-32&&x!=800-32){
			x+=Delta()*speed;
		} else {
			x = 800-32;
		}
	}
	
	public void shoot(){
		if(projectileManager.playerProjectiles()<this.maxShots&&!Clock.isPaused()){
			projectileManager.addProjectile(x+12, y-16, 400, QuickLoad("shot8x16"),true);
			SoundBoard.playSound("shot",0.5f);
		}
	}
	
	public boolean checkCollisionWithProjectile(Projectile projectile){
		Rectangle thisRect = new Rectangle((int)x, (int)y, 32, 32);
		Rectangle thatRect = new Rectangle((int)projectile.getX(), (int)projectile.getY(), 8, 16);
		if(thisRect.intersects(thatRect)){
			this.die();
			return true;
		}
		return false;
	}
	
	
	public void die(){
		alive = false;
	}
	
	public boolean isAlive(){
		return alive;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
}
