package data;

import java.util.ArrayList;

public class CoverSet {
	private float x, y;
	private ArrayList<CoverPiece> pieces;
	
	public CoverSet(float x, float y) {
		this.x = x;
		this.y = y;
		this.pieces = new ArrayList<CoverPiece>();
		this.createPieces();
	}
	
	private void createPieces(){
		//Row 1
		pieces.add(new CoverPiece(x, y, CoverPiece.coverSet(2)));
		pieces.add(new CoverPiece(x+16, y, CoverPiece.coverSet(1)));
		pieces.add(new CoverPiece(x+32, y, CoverPiece.coverSet(1)));
		pieces.add(new CoverPiece(x+48, y, CoverPiece.coverSet(2),90));
		
		//Row 2
		pieces.add(new CoverPiece(x, y+16, CoverPiece.coverSet(1)));
		pieces.add(new CoverPiece(x+16, y+16, CoverPiece.coverSet(2),180));
		pieces.add(new CoverPiece(x+32, y+16, CoverPiece.coverSet(2),270));
		pieces.add(new CoverPiece(x+48, y+16, CoverPiece.coverSet(1)));
		
		//Row 3
		pieces.add(new CoverPiece(x, y+32, CoverPiece.coverSet(1)));
		//nothing
		//nothing
		pieces.add(new CoverPiece(x+48, y+32, CoverPiece.coverSet(1)));
	}
	
	public void update(){
		
		for(CoverPiece p : this.pieces){
			p.update();
		}
	}
	
	

	public ArrayList<CoverPiece> getPieces() {
		return pieces;
	}
	
	
	
}
