package data;

import static helpers.Artist.*;
import static helpers.SoundBoard.*;

import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

import org.newdawn.slick.opengl.Texture;

import helpers.SoundBoard;

public class Enemy {
	
	private float x;
	private float y;
	private Texture texture;
	private boolean alive;
	private ProjectileManager projectileManager;
	private Game game;
	
	public Enemy(float x, float y, Texture texture, ProjectileManager projectileManager, Game game) {
		this.x = x;
		this.y = y;
		this.texture = texture;
		this.alive = true;
		this.projectileManager = projectileManager;
		this.game = game;
	}
	
	public boolean checkCollision(Projectile projectile){
		Rectangle thisRect = new Rectangle((int)x, (int)y, 32, 32);
		Rectangle thatRect = new Rectangle((int)projectile.getX(),(int)projectile.getY(),8,16);
		
		if(thisRect.intersects(thatRect)){
			alive = false;
			SoundBoard.playSound("explosion1");
			game.effectManager.addAnimation(new Animation(QuickLoad("explosion"), x, y, 32, 32, 0.008f));
			return true;
		}
		return false;
	}
	
	public void update(){
		if(alive){
			DrawQuadTex(texture, x, y, 32, 32);
		}
	}
	
	public void shoot(){
		
		projectileManager.addProjectile(x+12, y+32, -200, QuickLoad("enemyProjectile"),false);
	}
	
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public boolean isAlive() {
		return alive;
	}
	
	
	
}
