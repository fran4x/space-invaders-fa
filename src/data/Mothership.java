package data;

import static helpers.Artist.*;
import static helpers.Clock.*;
import static helpers.SoundBoard.playSound;

import java.awt.Rectangle;
import java.util.Random;

import org.newdawn.slick.opengl.Texture;

import ui.FlashingBonus;

public class Mothership {
	private float x, y, speed;
	private Texture texture;
	private boolean alive;
	private int score;
	private Game game;
	
	public Mothership(float x, float y, float speed, Texture texture, Game game) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.texture = texture;
		this.alive = true;
		this.score = (new Random().nextInt(6)+1) * 50;
		this.game = game;
		
	}
	
	public void update(){
		if(alive){
			x+=speed*Delta();
			
			this.draw();
		}
		if(x>800){
			alive = false;
		}
	}
	
	public boolean checkCollisionWithProjectile(Projectile projectile){
		Rectangle thisRect = new Rectangle((int)x, (int)y, 64, 32);
		Rectangle thatRect = new Rectangle((int)projectile.getX(),(int)projectile.getY(),8,16);
		
		if(thisRect.intersects(thatRect)){
			this.alive = false;
			game.score+=this.score;
			game.gameOverlay.addBonus(new FlashingBonus(x, y, 2, 0.01f, String.valueOf(this.score)));
			playSound("bonus",10);
			return true;
		}
		
		return false;
	}
	
	public void draw(){
		DrawQuadTex(texture, x, y, 64, 64);
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public int getScore() {
		return score;
	}
	
	
	
}
