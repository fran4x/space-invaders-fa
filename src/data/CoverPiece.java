package data;

import static helpers.Artist.DrawQuadTex;
import static helpers.Artist.QuickLoad;
import static helpers.SoundBoard.*;

import java.awt.Rectangle;

import org.newdawn.slick.opengl.Texture;

import helpers.Artist;

public class CoverPiece {
	private Texture[] destructionLevels;
	private float x, y;
	private int index;
	private boolean alive;
	private int rot;
	
	public CoverPiece(float x, float y, Texture[] destructionLevels, int rot) {
		this.destructionLevels = destructionLevels;
		this.index = 0;
		this.alive = true;
		this.x = x;
		this.y = y;
		this.rot = rot;
	}
	
	public CoverPiece(float x, float y, Texture[] destructionLevels) {
		this(x,y,destructionLevels, 0);
	}
	
	public static Texture[] coverSet(int id){
		Texture[] arr = new Texture[4];
		switch (id) {
		case 1:
			arr[0] = QuickLoad("1x1[0]");
			arr[1] = QuickLoad("1x1[1]");
			arr[2] = QuickLoad("1x1[2]");
			arr[3] = QuickLoad("1x1[3]");
			break;
		case 2:
			arr[0] = QuickLoad("1x1slope[0]");
			arr[1] = QuickLoad("1x1slope[1]");
			arr[2] = QuickLoad("1x1slope[2]");
			arr[3] = QuickLoad("1x1slope[3]");
			break;
		default:
			break;
		}
		return arr;
	}
	
	public void update(){
		if(alive){
			this.draw();
		}
	}
	
	public void draw(){
		Artist.DrawQuadTexRot(destructionLevels[index], x, y, 16, 16, rot);
	}
	
	public boolean checkProjectileCollision(Projectile projectile){
		Rectangle thisRect = new Rectangle((int)x, (int)y, 16, 16);
		Rectangle thatRect = new Rectangle((int)projectile.getX(), (int)projectile.getY(), 8, 16);
		if(thisRect.intersects(thatRect)){
			this.damage();
			return true;
		}
		return false;
	}
	
	public void damage(){
		index++;
		playSound("rock_cracking");
		if(index>=destructionLevels.length){
			alive = false;
		}
	}

	public boolean isAlive() {
		return alive;
	}
	
	
	
}
