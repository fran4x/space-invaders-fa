package data;

import static helpers.Clock.*;
import static helpers.SoundBoard.*;

import org.newdawn.slick.opengl.Texture;

public class MothershipGenerator {
	
	private float timeSinceLastGen, minimumTime, speed;
	private Mothership mothership;
	private Texture texture;
	private Game game;
	
	public MothershipGenerator(float minimumTime, float speed, Texture texture, Game game) {
		this.minimumTime = minimumTime;
		this.timeSinceLastGen = 0;
		this.speed = speed;
		this.texture = texture;
		this.game = game;
		this.mothership = null;
		generateMothership();
		mothership.setAlive(false);
	}
	
	public void update(){
		timeSinceLastGen+=Delta();
		if(!mothership.isAlive()&&timeSinceLastGen>minimumTime){
			timeSinceLastGen = 0;
			generateMothership();
			playSound("boatHorn");
		}
		mothership.update();
	}
	
	private void generateMothership(){
		
		this.mothership = new Mothership(-70, 30, speed, texture, game);
	}

	public Mothership getMothership() {
		return mothership;
	}
	
	
}
