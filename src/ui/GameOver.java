package ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import data.Boot;
import data.Game;
import data.State;
import helpers.HighScoreScribe;
import helpers.SoundBoard;

public class GameOver implements State{
	private TrueTypeFont font;
	private int score, highScore;
	private FlashingBonus newHigh;
	private Button mainMenu, exit;
	
	public GameOver(int score) {
		this.font = GameOverlay.createFont();
		this.score = score;
		this.highScore = HighScoreScribe.readHighScore();
		int duration=0;
		if(score>highScore){
			duration = -1;
			HighScoreScribe.writeHighScore(score);
		}
		newHigh = new FlashingBonus(32, 124, duration, 0.1f, "New high score!");
		mainMenu = new Button(32, 256, font, "Main menu");
		exit = new Button(32, 281, font, "Exit");
		SoundBoard.playSound("gameover");
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		Game.drawBackgroundStat();
		newHigh.update();
		mainMenu.update();
		exit.update();
		font.drawString(32, 84, "GAME OVER",Color.red);
		font.drawString(32, 164, "High score: "+highScore,Color.yellow);
		font.drawString(32, 186, "Your score: "+score,Color.yellow);
		if(mainMenu.beingClicked()){
			Boot.currentState = new MainMenu();
		}
		if(exit.beingClicked()){
			Boot.gameContinues = false;
		}
	}
}
