package ui;

import static helpers.Artist.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import data.Boot;
import data.Game;
import data.State;

public class MainMenu implements State {
	
	private TrueTypeFont font;
	private Button playButton;
	private Button exitButton;
	private Button fullscreen;
	private Button help;
	private Texture background;
	
	public MainMenu() {
		this.font = GameOverlay.createFont();
		this.playButton = new Button(32, 162, font, "Play!");
		this.fullscreen = new Button(30, 206, font, "Toggle Fullscreen");
		this.exitButton = new Button(32,288,font,"Exit");
		this.help = new Button(32,248,font,"Help");
		
		this.background = QuickLoad("space");
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		DrawQuadTex(background,0, 0, 1024, 1024);
		font.drawString(32, 122, "SPACE INVADERS",Color.yellow);
		font.drawString(32, 550, "Rendition made by: Francisco Ayala",Color.yellow);
		playButton.update();
		exitButton.update();
		fullscreen.update();
		help.update();
		if(playButton.beingClicked()){
			Boot.currentState = new Game();
		}
		if(exitButton.beingClicked()){
			Boot.gameContinues=false;
		}
		if(fullscreen.beingClicked()){
			Boot.setFullscreen();
		}
		if(help.beingClicked()){
			Boot.currentState = new Help(font);
		}
	}

}
