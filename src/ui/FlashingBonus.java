package ui;

import java.awt.Font;
import java.util.Random;

import static helpers.Clock.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

public class FlashingBonus {
	private float x,y;
	private float duration, flashSpeed,timeSinceLastFlash, deathTimer;
	private boolean continues;
	private String text;
	private Color currentColor;
	private TrueTypeFont font;
	
	public FlashingBonus(float x, float y, float duration, float flashSpeed, String text) {
		this.x = x;
		this.y = y;
		this.duration = duration;
		this.flashSpeed = flashSpeed;
		this.text = text;
		this.timeSinceLastFlash = flashSpeed+1;
		this.deathTimer = 0;
		this.continues = true;
		this.font = GameOverlay.createFont();
	}
	
	public void update(){
		if(continues){
			timeSinceLastFlash+=Delta();
			
			deathTimer +=Delta();
			
			if(timeSinceLastFlash>flashSpeed){
				this.currentColor = generateRandomColor();
				timeSinceLastFlash = 0;
			}
			if(deathTimer>=duration&&duration!=-1){
				continues = false;
			}
			font.drawString(x, y, text, currentColor);
		}
	}
	
	private Color generateRandomColor(){
		int r = new Random().nextInt(255);
		int g = new Random().nextInt(255);
		int b = new Random().nextInt(255);
		return new Color(r, g, b);
	}

	public boolean isContinues() {
		return continues;
	}
	
}
