package ui;

import java.awt.Font;
import java.io.InputStream;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

import data.Game;
import helpers.Clock;
import helpers.HighScoreScribe;

public class GameOverlay {
	private Game game;
	private TrueTypeFont font;
	private ArrayList<FlashingBonus> flashingBonuses;
	private int highScore;
	private Button pause;
	
	public GameOverlay(Game game) {
		this.game = game;
		this.font = createFont();
		this.flashingBonuses = new ArrayList<FlashingBonus>();
		this.highScore = HighScoreScribe.readHighScore();
		this.pause = new Button(700, 600-font.getHeight(), font, "Pause");
	}
	
	public static TrueTypeFont createFont(){
		Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
		TrueTypeFont font2 = new TrueTypeFont(awtFont, true);
		
		TrueTypeFont font = font2;
		
		try {
			InputStream inputStream	= ResourceLoader.getResourceAsStream("res/CommodorePixelized.ttf");
 
			Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont2 = awtFont2.deriveFont(23f); // set font size
			font = new TrueTypeFont(awtFont2, true);
 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return font;
	}
	
	public void addBonus(FlashingBonus bonus){
		this.flashingBonuses.add(bonus);
	}
	
	public void update(){
		font.drawString(0, 0, "Lives: "+game.lives,Color.yellow);
		font.drawString(175, 0, "Level: "+game.level,Color.yellow);
		font.drawString(350, 0, "Score: "+game.score,Color.yellow);
		font.drawString(0, 600-font.getHeight(), "High Score:"+this.highScore,Color.yellow);
		pause.update();
		if(pause.beingClickedOnce()){
			Clock.Pause();
		}
		updateBonus();
	}
	
	private void updateBonus(){
		for(FlashingBonus f : this.flashingBonuses){
			f.update();
		}
		
		ArrayList<FlashingBonus> toBeRemoved = new ArrayList<FlashingBonus>();
		
		for(FlashingBonus f : this.flashingBonuses){
			if(!f.isContinues()){
				toBeRemoved.add(f);
			}
		}
		this.flashingBonuses.removeAll(toBeRemoved);
	}
	
	

	public TrueTypeFont getFont() {
		return font;
	}
	
	
}
