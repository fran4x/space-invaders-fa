package ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import data.Boot;
import data.Game;
import data.State;

public class Help implements State {
	
	private TrueTypeFont font;
	private Button mainMenu;
	
	
	
	public Help(TrueTypeFont font) {
		this.font = font;
		this.mainMenu = new Button(32, 550, font, "Main Menu");
	}



	@Override
	public void update() {
		Game.drawBackgroundStat();
		font.drawString(32, 32, "W/A/S/D - Movement keys", Color.yellow);
		font.drawString(32, 64, "SPACE - Fire", Color.yellow);
		font.drawString(32, 96, "F - Toggle Fullscreen", Color.yellow);
		font.drawString(32, 128, "ESCAPE - Exit Game", Color.yellow);
		font.drawString(32, 160, "P - Pause", Color.yellow);
		font.drawString(32, 192, "M - Mute", Color.yellow);
		mainMenu.update();
		if(mainMenu.beingClicked()){
			Boot.currentState = new MainMenu();
		}
	}

}
