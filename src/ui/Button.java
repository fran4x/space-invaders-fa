package ui;

import java.awt.Rectangle;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import data.Boot;
import helpers.SoundBoard;

public class Button {
	private float x,y;
	private TrueTypeFont font;
	private String text;
	private Rectangle rect;
	
	public Button(float x, float y, TrueTypeFont font, String text) {
		this.x = x;
		this.y = y;
		this.font = font;
		this.text = text;
		this.rect = generateRect();
	}
	
	private Rectangle generateRect(){
		return new Rectangle((int)x, (int)y, font.getWidth(text), font.getHeight());
	}
	
	public void update(){
		if(mouseAbove()){
			font.drawString(x, y, text, Color.white);
		} else{
			font.drawString(x, y, text, Color.green);
		}
	}
	
	public boolean beingClicked(){
		if(mouseAbove()&&Mouse.isButtonDown(0)){
			SoundBoard.playSound("click");
			return true;
		}
		return false;
	}
	
	public boolean beingClickedOnce(){
		while(Mouse.next()){
			if(mouseAbove()&&Mouse.isButtonDown(0)&&Mouse.getEventButtonState()){
				SoundBoard.playSound("click");
				return true;
			}
		}
		return false;
	}
	
	private boolean mouseAbove(){
		return rect.contains(Mouse.getX(), Boot.HEIGHT-Mouse.getY()-1);
	}
}
